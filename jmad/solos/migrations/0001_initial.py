# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('albums', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Solo',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('artist', models.CharField(max_length=100)),
                ('instrument', models.CharField(max_length=50)),
                ('start_time', models.CharField(blank=True, max_length=20, null=True)),
                ('end_time', models.CharField(blank=True, max_length=20, null=True)),
                ('slug', models.SlugField(blank=True, null=True)),
                ('track', models.ForeignKey(to='albums.Track')),
            ],
            options={
                'ordering': ['track', 'start_time'],
            },
        ),
    ]
